<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();


Route::group(['middleware' => ['auth', 'web']], function () {
    Route::get('/', 'ProjectController@reloadProjectFormDb')->name('home');
    Route::get('/developerlist', 'ProjectController@developer')->name('developerList');
    Route::post('/updateWithSeparateDate', 'ProjectController@updateWithSeparateDate')->name('updateWithSeparateDate');
    Route::get('/getproject', 'ProjectController@getProject')->name('getProject');
    Route::get('/projectlist', 'ProjectController@withOutDeveloper')->name('projectList');
    Route::post('/projectshow', 'ProjectController@projectShow')->name('projectShow');
    Route::get('/reloadTaskFormDb', 'ProjectController@reloadTaskFormDb')->name('reloadTaskFormDb');
    Route::post('/updateProject/{id}', 'ProjectController@updateProject')->name('updateProject');
    Route::post('/developercheck', 'DeveloperController@checkBusy')->name('checkBusy');
    Route::get('/developercheck/{id?}', 'DeveloperController@checkBusyDevUnderDistribution')->name('checkBusyDevUnderDistribution');
    Route::get('/appointmentofdevelopers', 'DeveloperController@appointmentOfDevelopers')->name('appointmentOfDevelopers');
    Route::post('/download', 'ProjectController@download')->name('download');
    Route::match(['get', 'post'], '/project/add/task/{id?}', 'ProjectController@addTask')->name('addTask');
    Route::match(['get', 'post'], '/project/add/developer/{id?}', 'ProjectController@addDev')->name('addDev');
    Route::get('/taskedit', 'TaskController@edit')->name('taskEdit');
    Route::DELETE('/taskdestroy', 'TaskController@destroy')->name('taskDestroy');
    Route::resource('project', 'ProjectController');
    Route::resource('task', 'TaskController');
});
