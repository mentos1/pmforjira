/**
 * Created by mentos1 on 15.08.17.
 */
$('#add').click(function (e) {
    e.preventDefault();
    $("#mySelect > option").prop("selected", "selected");

    var f = document.getElementById("form1");
    f.submit();
});
$('#close').click(function (e) {
    e.preventDefault();
    $('#dialog').dialog('close');
});

$('#delete').click(function (e){
    e.preventDefault();
    var data = {
        "_token": csrf_token,
        "id" : $(this).attr('data-id')
    };
    var id = $(this).attr('data-id');
    $.ajax({
        type: "DELETE",
        url: taskDestroy,
        data: data,
        dataType: 'json',
        success: function (response) {
            window.location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.dir(this.url);
            console.dir(textStatus, errorThrown);
        }
    });
});

function add_D(){ // Our function called by your button
    var x = $("#jurors").val(); // This is a JQuery function that'll get the value of a element with the ID 'jurors' - our textbox
    $("#jurors").val(""); // Clear the textbox value
    if(x==""){} // If there's nothing in the textbox
    else{
        $("#mySelect").append("<option value='"+x+"'>"+x+"</option>" ); // Get our select and add a option with the value of our textbox value with the text of the textbox input
    }
}

$("#mySelect").change(function(e){ // This is called whenever the user selects a option
    var x = e.target.value; // Get the value of the selected option
    $("#mySelect option[value='"+x+"']").remove(); // Remove the selected option
});