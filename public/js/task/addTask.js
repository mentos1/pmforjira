/**
 * Created by mentos1 on 15.08.17.
 */
function includeJs(url) {
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
}
function includeCss(url) {
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    head.appendChild(link);
}


$( function() {
    $( "#sortable1, #sortable2" ).sortable({
        connectWith: ".connectedSortable"
    }).disableSelection();

    $('img').click(function (e) {
        var data = {
            "_token": csrf_token,
            "project_id" : $("#project_Owner").attr("data-id"),
            "id" : $(this).attr('data-id')
        };
        $.ajax({
            type: "POST",
            url: addTask,
            data: data,
            dataType: 'json',
            success: function (response) {
                $("#div1").html(response.response);
                includeJs(linkTaskJs);
                includeCss(linkTaskCss);
                $('#dialog').dialog('open');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(textStatus, errorThrown);
            }
        });
    });

    $( "#dialog" ).dialog({
        autoOpen: false,
        width: "45%",
        maxWidth: "400px",
        minWidth: "400px",
        modal: true,
        weekends: false,
        show:{
            effect: 'blind',
            duration: 500
        },
        hide:{
            effect: 'blind',
            duration: 500
        },
    });



    $('#addNewTask').click(function () {
        var data = {
            "_token": csrf_token,
            "project_id": $("#project_Owner").attr("data-id"),
            "id" : $(this).attr('data-id')
        };
        $.ajax({
            type: "POST",
            url: addTask,
            data: data,
            dataType: 'json',
            success: function (response) {
                $("#div1").html(response.response);
                includeJs(linkTaskJs);
                includeCss(linkTaskCss);
                $('#dialog').dialog('open');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(textStatus, errorThrown);
            }
        });
    });

    $("#butSendToDb").click(function () {
        var d = $('#sortable2').find('img');
        var arr_idTask_for_send_to_controler = [];
        for(var i = 0; i < d.length; i++){
            arr_idTask_for_send_to_controler.push($($('#sortable2').find('img')[i]).attr('data-id'));
        }
        var data = {
            "_token": csrf_token,
            "tasks": arr_idTask_for_send_to_controler,
            "id" : $("#project_Owner").attr("data-id")
        };

        $.ajax({
            type: "GET",
            url: taskEdit,
            data: data,
            dataType: 'json',
            success: function (response) {
                $('.alert-success').attr("style","display: block")

                function func() {
                    $('.alert-success').hide("slow" );
                }

                setTimeout(func, 2000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.dir(this.url);
                console.dir(textStatus, errorThrown);
                $('.alert-danger').attr("style","display: block");

                function func() {
                    $('.alert-danger').hide("slow" );
                }

                setTimeout(func, 2000);
            }
        });
    });

} );