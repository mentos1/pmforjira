/**
 * Created by mentos1 on 26.07.17.
 */
$( "#dialog1" ).dialog({
    autoOpen: false,
    width: "45%",
    maxWidth: "40%",
    minWidth: "50%",
    modal: true,
    weekends: false,
    show:{
        effect: 'fade',
        duration: 500
    },
    hide:{
        effect: 'fade',
        duration: 500
    },
});
$( "#dialog2" ).dialog({
    autoOpen: false,
    width: "45%",
    maxWidth: "40%",
    minWidth: "50%",
    modal: true,
    weekends: false,
    show:{
        effect: 'fade',
        duration: 500
    },
    hide:{
        effect: 'fade',
        duration: 500
    },
});
