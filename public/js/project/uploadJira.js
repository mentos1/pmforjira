/**
 * Created by mentos1 on 26.07.17.
 */
function getMetaContent() {
    var metas = document.getElementsByTagName('meta');
    for(var i=0; i<metas.length; i++) {
        if(metas[i].getAttribute("name") == "csrf-token")
            return metas[i].getAttribute("content");
    }

    return "";
}


$('#downloadFromJira').click(function () {
    var data = {
        "_token": getMetaContent()
    };
    var $button = $(this);
    $button.removeClass('added').attr('disabled', 'disabled').attr('src', urlLoadPng);

    $.ajax({
        type: "POST",
        url: $(this).attr('data-url'),
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.response) {
                reloadTaskFormDb();
                if ($('.fc-timelineDay-button , .fc-day-header > a ,  .fc-day-number , .fc-timelineSevenDay-button').hasClass('fc-state-active')) {
                    $('#calendar').fullCalendar('removeEventSource', {events: projectList});
                    $('#calendar').fullCalendar('removeEventSource', {events: project});
                    $('#calendar').fullCalendar('addEventSource', project);
                }
                if ($('.fc-month-button , .fc-agendaWeek-button').hasClass('fc-state-active')) {
                    $('#calendar').fullCalendar('removeEventSource', {events: project});
                    $('#calendar').fullCalendar('removeEventSource', {events: projectList});
                    $('#calendar').fullCalendar('addEventSource', projectList);
                }

                $('#calendar').fullCalendar({
                    resources: developerList
                });
                $button.removeAttr('disabled').attr('src', urlReloadPng);
                $('.alert-success').attr("style","display: block")

                function func() {
                    $('.alert-success').hide("slow" );
                }

                setTimeout(func, 2000);
            } else {
                $button.removeAttr('disabled').attr('src', urlReloadPng);
                $('.alert-danger').attr("style","display: block");

                function func() {
                    $('.alert-danger').hide("slow" );
                }
                setTimeout(func, 2000);
                alert("Error.");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $button.removeAttr('disabled').attr('src', urlReloadPng);
            $('.alert-danger').attr("style","display: block");

            function func() {
                $('.alert-danger').hide("slow" );
            }
            setTimeout(func, 2000);
            console.log(textStatus, errorThrown);
        }
    });
});

setInterval(function(){
    // sent ajax to Controller for upload from Jira
    $("#downloadFromJira").trigger('click');
}, 60000 * 10);