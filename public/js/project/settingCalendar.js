/**
 * Created by mentos1 on 26.07.17.
 */
/*includeJs("../public/lib/timepicker/jquery-ui-timepicker-addon.js");
includeCss("../public/lib/timepicker/jquery-ui-timepicker-addon.css");*/

function includeJs(url) {
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
}
function includeCss(url) {
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    head.appendChild(link);
}


includeJs(timePickerJs);
includeCss(timePickerCss);

$('#calendar').fullCalendar({
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
    now: new Date(),
    navLinks: true,
    textColor: '#333',
    nowIndicator: true,
    resourceAreaWidth: '25%',
    editable: true, // enable draggable events
    aspectRatio: 1.8,
    minTime: "10:00:00",
    maxTime: "19:00:00",
    //weekends: false,
    businessHours:[{
        // days of week. an array of zero-based day of week integers (0=Sunday)
        dow: [ 1, 2, 3, 4, 5], // Monday - Thursday

        start: '10:00', // a start time (10am in this example)
        end: '19:00', // an end time (6pm in this example)
    }, {
        dow: [ 0, 6 ], // Thursday, Friday
        start: '00:00', // 10am
        end: '00:00' // 4pm
    }],
    scrollTime: '00:00', // undo default 6am scrollTime
    header: {
        left: 'today prev,next',
        center: 'title',
        right: 'timelineDay,timelineSevenDay,month'
    },
    dayClick: function(date, jsEvent, view) {
        if ($('#calendar').fullCalendar('getView').type == 'month') {
            var checkDay = moment(moment(date._d).format('YYYY-MM-DD'));
            if (checkDay.day() == 6 || checkDay.day() == 0) {
                alert('Weekend!');
            } else {
                $('#start-date').val(moment(date._d).format('YYYY-MM-DD')); //make papse date
                $('#start-date').datepicker("option", 'minDate', '' + moment(date._d).format('YYYY-MM-DD'));
                $('#end-date').val(moment(date._d).format('YYYY-MM-DD'));
                $('#end-date').datepicker("option", 'minDate', '' + moment(date._d).format('YYYY-MM-DD'));
                $('#start-time').val(+moment(date._d).format('HH') - 3 + ":00"); // -3h for correct date
                $('#dialog').dialog('open');

            }
        }
    },
    defaultView: 'timelineDay',
    views: {
        timelineSevenDay: {
            type: 'timeline',
            duration: { day: 7 },
            slotDuration: '24:00'
        }
    },
    resourceLabelText: 'Developers',
    resources: {
        url: developerList,
        error: function() {
            $('#resource-warning').show();
        }
    },
    eventSources: [ project ],
    eventRender: function(event, element) {
        element.attr('href', 'javascript:void(0);');
        element.click(function () {
            var data = {
                "_token": getMetaContent(),
                "id" : event.id
            };
            $("#delete").attr('date-id',event.id);
            $.ajax({
                type: "GET",
                url: getProject,
                data: data,
                dataType: 'json',
                success: function (response) {
                    console.log(response.response);
                    includeJs(infoProject);
                    $("#div1").html(response.response);
                    if($('.fc-timelineDay-button , .fc-day-header > a ,  .fc-day-number , .fc-timelineSevenDay-button').hasClass('fc-state-active')){
                        $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
                        $('#calendar').fullCalendar( 'removeEventSource', { events: project});
                        $('#calendar').fullCalendar( 'addEventSource',  project);
                    }
                    if($('.fc-month-button , .fc-agendaWeek-button').hasClass('fc-state-active')){
                        $('#calendar').fullCalendar( 'removeEventSource', { events: project});
                        $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
                        $('#calendar').fullCalendar( 'addEventSource',  projectList);
                    }

                    $('#dialog1').dialog('open');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.dir(textStatus, errorThrown);
                }
            });
        });
    },
    eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
        var data = {
            "_token": getMetaContent(),
            "id" : event.id,
            "start": event.start.format("YYYY-MM-DD H(:mm)"),
            "end": event.end.format("YYYY-MM-DD H(:mm)")
        };
        var start_m = moment(event.start.format("YYYY-MM-DD"));
        var end_m = moment(event.end.format("YYYY-MM-DD"));
        if ((start_m.day() == 6 || start_m.day() == 0) || (end_m.day() == 6 || end_m.day() == 0)) {
            alert('Weekend!');
            revertFunc();
        }else {
            if((data.start.split(' ')[1].split(':')[0] >= 10 && data.start.split(' ')[1].split(':')[0] <= 18) && (data.end.split(' ')[1].split(':')[0] >= 10 && data.end.split(' ')[1].split(':')[0] <= 19)){
                $.ajax({
                    type: "post",
                    url: checkBusy,
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        console.log(response.response);
                        if (response.response) {
                            $.ajax({
                                type: "POST",
                                url: projectShow,
                                data: data,
                                dataType: 'json',
                                success: function (response) {
                                    if (response) {
                                        if ($('.fc-timelineDay-button , .fc-day-header > a ,  .fc-day-number , .fc-timelineSevenDay-button').hasClass('fc-state-active')) {
                                            $('#calendar').fullCalendar('removeEventSource', { events: projectList });
                                            $('#calendar').fullCalendar('removeEventSource', { events: project});
                                            $('#calendar').fullCalendar('addEventSource', project);
                                        }
                                        if ($('.fc-month-button , .fc-agendaWeek-button').hasClass('fc-state-active')) {
                                            $('#calendar').fullCalendar('removeEventSource', {events: project});
                                            $('#calendar').fullCalendar('removeEventSource', {events: projectList});
                                            $('#calendar').fullCalendar('addEventSource', projectList);
                                        }

                                    } else {
                                        alert("Ошибка добавления.");
                                    }

                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }
                            });
                        } else {
                            alert("Developer is busy.");
                            revertFunc();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }else{
                if(!((data.start.split(' ')[1].split(':')[0] >= 10 && data.start.split(' ')[1].split(':')[0] <= 18) || (data.end.split(' ')[1].split(':')[0] == 19 && data.end.split(' ')[1].split(':')[1]  === undefined))){
                    alert("Data start is non-business hours");
                    revertFunc();
                }
                if(!((data.end.split(' ')[1].split(':')[0] >= 10 && data.end.split(' ')[1].split(':')[0] <= 18) || (data.end.split(' ')[1].split(':')[0] == 19 && data.end.split(' ')[1].split(':')[1]  === undefined))){
                    alert("Data end is non-business hours");
                    revertFunc();
                }
            }
        }
    },
    eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {
        var data = {
            "_token": getMetaContent(),
            "id" : event.id,
            "start": event.start.format("YYYY-MM-DD H(:mm)"),
            "end": event.end.format("YYYY-MM-DD H(:mm)")
        };

        var start_m = moment(event.start.format("YYYY-MM-DD"));
        var end_m = moment(event.end.format("YYYY-MM-DD"));
        if ((start_m.day() == 6 || start_m.day() == 0) || (end_m.day() == 6 || end_m.day() == 0)) {
            alert('Weekend!');
            revertFunc();
        }else {
            $.ajax({
                type: "POST",
                url:  projectShow,
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response) {
                        if ($('.fc-timelineDay-button , .fc-day-header > a ,  .fc-day-number , .fc-timelineSevenDay-button').hasClass('fc-state-active')) {
                            $('#calendar').fullCalendar('removeEventSource', {events: projectList});
                            $('#calendar').fullCalendar('removeEventSource', {events: project});
                            $('#calendar').fullCalendar('addEventSource', project);
                        }
                        if ($('.fc-month-button , .fc-agendaWeek-button').hasClass('fc-state-active')) {
                            $('#calendar').fullCalendar('removeEventSource', {events: project});
                            $('#calendar').fullCalendar('removeEventSource', {events: projectList});
                            $('#calendar').fullCalendar('addEventSource', projectList);
                        }
                    } else {
                        alert("Error added.");
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
        //revertFunc(); отменяет действие
    }
}).on('click', '.fc-month-button ', function() {
    localStorage.setItem("class", ".fc-month-button");
    $('#calendar').fullCalendar( 'removeEventSource', { events: project});
    $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
    $('#calendar').fullCalendar( 'addEventSource',  projectList);
}).on('click', '.fc-timelineSevenDay-button', function() {
    localStorage.setItem("class", ".fc-timelineSevenDay-button");
    $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
    $('#calendar').fullCalendar( 'removeEventSource', { events: project});
    $('#calendar').fullCalendar( 'addEventSource',  project);
}).on('click', '.fc-day-number', function() {
    localStorage.setItem("class", ".fc-timelineSevenDay-button");
    $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
    $('#calendar').fullCalendar( 'removeEventSource', { events: project});
    $('#calendar').fullCalendar( 'addEventSource',  project);
}).on('click', '.fc-timelineDay-button', function() {
    localStorage.setItem("class", ".fc-timelineDay-button");
    $('#calendar').fullCalendar( 'removeEventSource', { events: projectList});
    $('#calendar').fullCalendar( 'removeEventSource', { events: project});
    $('#calendar').fullCalendar( 'addEventSource',  project);
});

