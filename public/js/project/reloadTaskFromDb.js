/**
 * Created by mentos1 on 15.08.17.
 */
function reloadTaskFormDb() {
    $.ajax({
        url: urlReload,
        dataType:'json',
        type:'GET',
        success:function(html){
            $('#dialog').empty();
            $('#dialog').html(html.response);
            includeJs(timePickerJs);
            includeCss(timePickerCss);
            includeJs(settingTimePicker);/*
            $('.ui-timepicker-input').timepicker({
                minTime: "10:00",
                maxTime: "19:00"
            } );*/
            $( "#dialog" ).dialog({
                autoOpen: false,
                width: "45%",
                maxWidth: "40%",
                minWidth: "50%",
                modal: true,
                weekends: false,
                show:{
                    effect: 'blind',
                    duration: 500
                },
                hide:{
                    effect: 'blind',
                    duration: 500
                },
            });

            $('#end-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends });
            $('#start-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends});

            $('.clockpicker').clockpicker({
                autoclose: true,
            });

            $('#open_select_from_jira').click(function() {
                if (!$(this).is(':checked')) {
                    $('#project').attr('disabled', true);
                    $('#project').attr("required", false);
                    $('#nameProject').prop('readonly', false);
                    $('#nameProject').attr("required", true);
                }else{
                    $('#project').attr('disabled', false);
                    $('#project').attr('required', true);
                    $('#nameProject').prop('readonly', true);
                    $('#nameProject').attr("required", false);
                }
            }).click();
        }
    });
}