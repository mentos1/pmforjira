/**
 * Created by mentos1 on 15.08.17.
 */
$(this).ready(function () {
    $('#end-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends });
    $('#start-date').datepicker({ dateFormat: 'yy-mm-dd', beforeShowDay: $.datepicker.noWeekends});
});

$('.ui-timepicker-input').timepicker({
    minTime: "10:00",
    maxTime: "19:00"
} );



