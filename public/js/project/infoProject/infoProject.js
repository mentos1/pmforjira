$("#delete").on('click',function (e) {
    var data = {
        "_token": csrf_token
    };

    var url = $(this).attr("data-url");
    $.ajax({
        type: "DELETE",
        url: url,
        data: data,
        dataType: 'json',
        success: function (response) {
            if (response.response) {
                window.location.replace(BASEURL);
            } else {
                alert("Ошибка Удаления.");
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
});

$('#updateInfo').click(function (e) {
    var data = {
        "_token": csrf_token
    };
    var url = $(this).attr("data-url");
    $.ajax({
        type: "post",
        url: url,
        data: data,
        dataType: 'json',
        success: function (response) {
            $('#dialog2').empty();
            $("#dialog2").html(response.response);
            includeJs(updateProject);
            includeJs(timePickerJs);
            includeCss("//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
            includeCss(timePickerCss);
            $("#dialog1").dialog('close');
            $("#dialog2").dialog('open');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.dir(textStatus, errorThrown);
        }
    });
});


$(".add_img").click(function () {
        var f = document.createElement("form");
        f.setAttribute('method',"get");
        f.setAttribute('id',"get");
        f.setAttribute('action', $(this).attr("data-url"));


        var s = document.createElement("input"); //input element, Submit button
        s.setAttribute('type',"submit");
        s.setAttribute('value',"Submit");

        f.appendChild(s);
        document.getElementsByTagName('body')[0].appendChild(f);
        f.submit();
});