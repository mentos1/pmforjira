<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NoneValueSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    /*    DB::table('description_projects')->insert([
            'name' => 'none'
        ]);
        DB::table('description_task')->insert([
            'name' => 'none'
        ]);*/
        DB::table('projects')->insert([
            'title' => 'none',
            'start' => '1900-01-01 00:00:00', // спецыальная дата для определения проэкта пустышки
                                              // на который ссылаются свободные такси и программисты
            'end' => '1900-01-01 00:00:00'
        ]);
    }
}
