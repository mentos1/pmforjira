<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTagSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_specialie', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('speciality_id')->unsigned()->default(1);
            $table->foreign('speciality_id')->references('id')->on('specialies')->onDelete('cascade');

            $table->integer('task_id')->unsigned()->default(1);
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_specialie');
    }
}
