<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeveloperTagSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developer_specialie', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('developer_id')->unsigned()->default(1);
            $table->foreign('developer_id')->references('id')->on('developers')->onDelete('cascade');

            $table->integer('speciality_id')->unsigned()->default(1);
            $table->foreign('speciality_id')->references('id')->on('specialies')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developer_specialie');
    }
}
