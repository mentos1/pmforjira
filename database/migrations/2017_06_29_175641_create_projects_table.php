<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->string('projectTypeKey', 100)->nullable();
            $table->string('key', 100)->default(0);

            /*$table->integer('description_id')->unsigned()->default(1);
            $table->foreign('description_id')->references('id')->on('description_projects')->onDelete('cascade');*/

            $table->string('lead', 100)->nullable();

            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->integer('estimate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
