<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $table = 'tasks';
    protected $fillable = ['id','summary','key','color','priority_id','project_id','resolution','status'];


    public function project(){
        return $this->belongsTo('App\Project');
    }

    public function priority(){
        return $this->hasOne('App\PriorityTask','id', 'priority_id');
    }

    public function descriptions(){
        return $this->hasMany('App\DescriptionTask','id', 'description_id');
    }

    public function specialities(){
        return $this->belongsToMany('App\Specialie','task_specialie', 'task_id', 'speciality_id');
    }
}
