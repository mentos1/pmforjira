<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;


class DeveloperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'developer';
    }
}