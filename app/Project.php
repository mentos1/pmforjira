<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DescriptionProject;

class Project extends Model
{
    protected $fillable = ['id','title','projectTypeKey','key','description_id','lead','start','end','estimate'];

    public function descriptions(){
        return $this->hasMany('App\DescriptionProject','project_id','id'); // best exam
    }

    public function tasks(){
        return $this->hasMany('App\Task','project_id','id');
    }

    public function developers(){
        return $this->belongsToMany('App\Developer','project_developer', 'project_id', 'developer_id');
    }
}
