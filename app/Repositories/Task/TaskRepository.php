<?php

namespace App\Repositories\Task;

interface TaskRepository
{
    //main methods
    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll();

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id);

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id);

    /**
     * Create a new data.
     *
     * @param  array  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data);

    //custom method
    /**
     * @param $myValue
     * @return mixed
     */
    public function myMethod($myValue);

    /**
     * This method assigns tasks to the project.
     *
     * @param array $arrTask
     * @param integer $id
     * @return array
     *
     * Returns an array of tasks assigned to the project
     */
    public function appointmentOfTasks( $arrTask, $id );

    /**
     * This method creates or updates a task.
     *
     * @param integer $id
     * @param string $summary
     * @param string $key
     * @param string $resolution
     * @param string $status
     * @param string $color
     * @param integer $priorityId
     * @param integer $projectId
     * @param string $descriptions
     * @param string $labels
     *
     * Nothing returns
     */
    public function createOrUpdate(
        $id ,
        $summary,
        $key,
        $resolution,
        $status,
        $color,
        $priorityId,
        $projectId,
        $descriptions,
        $labels
    );

}