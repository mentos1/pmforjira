<?php

namespace App\Repositories\Task;

use App\DescriptionTask;
use App\Repositories\Repository;
use App\Specialie;
use App\Task;
use App\Project;

class EloquentTaskRepository extends Repository implements TaskRepository
{

    public function myMethod($myValue)
    {
        $test = $this->model->where('my_condition', '=', $myValue);
    }

    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Create a new data.
     *
     * @param  array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * This method assigns tasks to the project.
     *
     * @param array $arrTask
     * @param integer $id
     * @return array
     *
     * Returns an array of tasks assigned to the project
     */
    public function appointmentOfTasks( $arrTask, $id )
    {
        $project= Project::find($id);
        $arrId = [];
        foreach ($project->tasks as $task) {
            array_push($arrId , $task->id);
        }

        //reset
        if (count($arrId) != 0) {
            $tasks = Task::whereIn('id',$arrId);
            $tasks->update(['project_id' => 1]);
        }

        //set
        if (count($arrTask) != 0) {
            $tasks = Task::whereIn('id',$arrTask);
            $tasks->update(['project_id' => $id]);
        }



        return $project->tasks;
    }


    /**
     * This method creates or updates a task.
     *
     * @param integer $id
     * @param string $summary
     * @param string $key
     * @param string $resolution
     * @param string $status
     * @param string $color
     * @param int $priorityId
     * @param string $descriptions
     * @param string $labels
     * @param int $projectId
     *
     * Nothing return
     */
    public function createOrUpdate(
        $id,
        $summary,
        $key,
        $resolution,
        $status,
        $color,
        $priorityId,
        $descriptions,
        $labels,
        $projectId = 1
    ) {
        if ($id != null) {
            $newTask = Task::find($id);
        }
        else {
            $newTask = new Task();
            $newTask->project_id = $projectId;
        }

        $newTask->summary = $summary;
        $newTask->key = $key;
        $newTask->resolution = $resolution;
        $newTask->status = $status;
        $newTask->color = $color;
        $newTask->priority_id = $priorityId;

        $newTask->save();

        if ($descriptions !== null) {
            $desTask = new DescriptionTask();
            $desTask->name = $descriptions;
            $desTask->task_id = $newTask->id;
            $desTask->save();
        }

        $result = [];
        if (isset($labels)) {
            $arrIdSpeciality = Specialie::whereIn('name', $labels)->pluck('id')->toArray(); // new whereIn
            $arrNameSpeciality = Specialie::whereIn('name', $labels)->pluck('name')->toArray(); // new whereIn
            $arrUnique = array_diff($labels, $arrNameSpeciality);

            $data = array();
            foreach ($arrUnique as $item) {
                array_push($data, ["name" => $item]);
            }

            Specialie::insert($data);
            $result = array_merge($arrIdSpeciality, Specialie::whereIn('name', $arrUnique)->pluck('id')->toArray());
        }

        $taskSpeciality = Task::find($newTask->id);
        $taskSpeciality->specialities()->detach();
        $taskSpeciality->specialities()->attach($result);
    }
}