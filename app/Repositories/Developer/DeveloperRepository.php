<?php

namespace App\Repositories\Developer;

interface DeveloperRepository
{
    //main methods
    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll();

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id);

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id);

    /**
     * Create a new data.
     *
     * @param  array  $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data);

    //custom method
    /**
     * @param $myValue
     * @return mixed
     */
    public function myMethod($myValue);

    /**
     * When you change the date of the project for which it has developers.
     * The method is called "checkBusy" checking the busy programmer in this interval of time.
     *
     * @param string $start
     * @param string $end
     * @param integer $id
     * @return boolean
     *
     * Return boolean value
     */
    public function checkBusy($start, $end, $id);

    /**
     * This method makes the date format Y-m-d H:i:s
     *
     * @param string $dateCreate
     * @return carbon
     *
     * Return carbon data
     */
    public function CreateDateFormat($dateCreate);

    /**
     * When the developer is assigned to the project,
     * the method "checkBusyDevUnderDistribution"
     * is used to check the free programmer at this time.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public function checkBusyDevUnderDistribution($arrDeveloper,$id);

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public function appointmentOfDevelopers($arrDeveloper, $id);


}