<?php

namespace App\Repositories\Developer;

use App\Developer;
use App\Project;
use App\Repositories\Repository;
use Carbon\Carbon;

class EloquentDeveloperRepository extends Repository implements DeveloperRepository
{

    public function myMethod($myValue)
    {
        $test = $this->model->where('my_condition', '=', $myValue);
    }

    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Create a new data.
     *
     * @param  array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * When you change the date of the project for which it has developers.
     * The method is called "checkBusy" checking the busy programmer in this interval of time.
     *
     * @param string $start
     * @param string $end
     * @param integer $id
     * @return boolean
     *
     * Return boolean value
     */
    public  function checkBusy($start, $end, $id)
    {
        $start =  $this->CreateDateFormat($start);
        $end =  $this->CreateDateFormat($end);

        $arrDev = Project::find($id)->developers->pluck('id')->toArray();
        $developers = $this->getBusyDevelopers($arrDev, $start, $end, $id);

        if (count($developers) !== 0) {
            return false;
        }
        return true;
    }


    /**
     * This method makes the date format Y-m-d H:i:s
     *
     * @param string $dateCreate
     * @return carbon
     *
     * Return carbon data
     */
    public function CreateDateFormat($dateCreate)
    {
        $date = explode(" ", $dateCreate);
        $startTime = isset($date[1]) ? $date[1] : "";
        $arrTime = explode(":", $startTime);
        $start = "0000-00-00 00:00:00.000000";

        if (count($arrTime) == 0) $start = Carbon::createFromFormat('Y-m-d', $dateCreate);

        if (count($arrTime) == 1) $start = Carbon::createFromFormat('Y-m-d H', $dateCreate);

        if (count($arrTime) == 2) $start = Carbon::createFromFormat('Y-m-d H:i', $dateCreate);

        if (count($arrTime) == 3) $start = Carbon::createFromFormat('Y-m-d H:i:s', $dateCreate);
        return $start;
    }

    /**
     * When the developer is assigned to the project,
     * the method "checkBusyDevUnderDistribution"
     * is used to check the free programmer at this time.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public function checkBusyDevUnderDistribution($arrDeveloper,$id)
    {
        $arr = [];

        if (isset($arrDeveloper)) {
            $start = $this->CreateDateFormat(Project::find($id)->start);
            $end = $this->CreateDateFormat(Project::find($id)->end);
            $developers = $this->getBusyDevelopers($arrDeveloper, $start, $end, $id);
            foreach($developers as $item) {
                array_push($arr, $item->title);
            }
        }

        return $arr;
    }

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param carbon $start
     * @param carbon $end
     * @param integer $id
     * @return array Returns an array with busy developers
     *
     * Returns an array with busy developers
     */
    function getBusyDevelopers($arrDeveloper, $start, $end, $id)
    {
        return Developer::whereIn("id", $arrDeveloper)
            ->with('projects')
            ->whereHas('projects', function($q) use($start, $end, $id)
            {
                $q->where([
                    ['projects.id','<>', $id],
                    ['projects.start','>=', $start],
                    ['projects.start','<=', $end],
                ])
                    ->orWhere([
                        ['projects.id','<>', $id],
                        ['projects.end', '>=', $start],
                        ['projects.end', '<=', $end],
                    ]);
            })
            ->get();
    }


    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public function appointmentOfDevelopers($arrDeveloper, $id)
    {
        $project = Project::find($id);
        $project->developers()->detach();
        $project->developers()->attach($arrDeveloper);
        return $project->developers;
    }

}