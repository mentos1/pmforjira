<?php

namespace App\Repositories\Project;

use App\DescriptionProject;
use App\Developer;
use App\Project;
use App\Repositories\Repository;

class EloquentProjectRepository extends Repository implements ProjectRepository
{

    public function myMethod($myValue)
    {
        $test = $this->model->where('my_condition', '=', $myValue);
    }

    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * This method returns all projects
     * that are assigned to the developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public function getAllProjectAndDeveloper()
    {
        $arr = [];
        $developers = Developer::with('projects')
            ->whereHas('projects', function($q)
                {
                    $q->where('start', '<>', null)
                        ->orWhere('end', '<>', null);
                })
            ->get(['id', 'resourceId']);

        //with a resourceId
        foreach ($developers as $developer) {
            if (isset($developer->projects)) {
                foreach ($developer->projects as $it) {
                    $it['resourceId'] = $developer->id;
                    array_push($arr, $it);
                }
            }
        }

        $projects = Project::with('developers')
            ->where('start', '<>', null)
            ->where('end', '<>', null)
            ->get(['id', 'title', 'start', 'end']);

        //with out a resourceId
        foreach($projects as $it) {
            if (!sizeof($it->developers)) {
                array_push($arr, $it);
            }
        }
        return $arr;
    }

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public function getAllProjectWithOutDeveloper()
    {

        $projects = Project::get(['id', 'title', 'start', 'end'])
            ->where('start', '<>', null)
            ->where('end', '<>', null)
            ->toArray();

        return $projects;
    }

    /**
     * This method returns all developers
     *
     * @return array
     *
     * Returns an array developers
     */
    public function getDeveloper()
    {
        return Developer::get(['id', 'title'])->toArray();
    }

    /**
     * This method create project
     *
     * @param Project $project
     * @param string $title
     * @param string $key
     * @param string $projectTypeKey
     * @param string $lead
     * @param string $start
     * @param string $end
     * @param null $descriptions
     *
     * Nothing returns
     */
    public function create(
        $project,
        $title,
        $key,
        $projectTypeKey,
        $lead,
        $start = null,
        $end = null,
        $descriptions = null
    ) {

        $title != null ? $project->title = $title : false;
        $lead != null ? $project->lead = $lead : false;
        $key != null ? $project->key = $key : false;
        $projectTypeKey !== null ? $project->projectTypeKey = $projectTypeKey : false;
        $project->start = $start;
        $project->end = $end;
        $project->save();

        if ($descriptions != null) {
            DescriptionProject::where('project_id', $project->id)->delete();
            $descriptionNew = new DescriptionProject();
            $descriptionNew->name = $descriptions;
            $descriptionNew->project_id = $project->id;
            $descriptionNew->save();
        }
    }

    /**
     * This method update project
     *
     * @param $id
     * @param string $start
     * @param string $end
     *
     * Nothing returns
     */
    public function updateDateProject($id,  $start, $end)
    {
        $msg = Project::find($id);
        $msg->start = $start;
        $msg->end = $end;
        $msg->save();
    }
    
    
    

}