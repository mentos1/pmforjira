<?php

namespace App\Repositories\Project;

interface ProjectRepository
{
    //main methods
    /**
     * Get all data.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll();

    /**
     * Find data by given an identifier.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id);

    /**
     * Delete a specified data by given data id.
     *
     * @param  int $id
     * @return boolean
     */
    public function delete($id);

    //custom method
    /**
     * @param $myValue
     * @return mixed
     */
    public function myMethod($myValue);

    /**
     * This method returns all projects
     * that are assigned to the developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public function getAllProjectAndDeveloper();

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public function getAllProjectWithOutDeveloper();

    /**
     * This method returns all developers
     *
     * @return array
     *
     * Returns an array developers
     */
    public function getDeveloper();

    /**
     * This method create project
     *
     * @param Project $project
     * @param string $name
     * @param string $key
     * @param string $projectTypeKey
     * @param string $lead
     * @param string $start
     * @param string $end
     * @param $distribution
     *
     *
     * @return
     * Nothing returns
     */
    public function create(
        $project,
        $name,
        $key,
        $projectTypeKey,
        $lead,
        $start,
        $end,
        $distribution
    );

    /**
     * This method update project
     *
     * @param $id
     * @param string $start
     * @param string $end
     *
     * Nothing returns
     */
    public function updateDateProject($id,  $start, $end);



}