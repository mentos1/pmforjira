<?php

namespace App\Providers;

use App\Helpers\DeveloperDb;
use Illuminate\Support\ServiceProvider;

class DeveloperProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('developer', function(){
            return new DeveloperDb();
        });
    }
}
