<?php
namespace App\Providers;

use App\Developer;
use App\Project;
use App\Repositories\Project\EloquentProjectRepository;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Task\EloquentTaskRepository;
use App\Repositories\Task\TaskRepository;
use App\Repositories\Developer\DeveloperRepository;
use App\Repositories\Developer\EloquentDeveloperRepository;
use App\Task;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //здесь мы будем указывать, какие репозитории биндить к интерфейсу
        // dependency injection во всей своей красе
        $this->app->bind(TaskRepository::class, function($app) {
            return new EloquentTaskRepository(new Task());
        });

        $this->app->bind(DeveloperRepository::class, function($app) {
            return new EloquentDeveloperRepository(new Developer());
        });

        $this->app->bind(ProjectRepository::class, function($app) {
            return new EloquentProjectRepository(new Project());
        });


    }
}