<?php

namespace App\Presenters;

class TaskPresenter
{
    public function formatData($data)
    {
        //форматируем данные по своему усмотрению
        return [
            'data1' => $data->test1,
            'data2' => $data->test2,
            'data3' => $data->test3,
            'data4' => $data->test4,
            'data5' => $data->test5,
            'data6' => $data->test6,
        ];
    }
}