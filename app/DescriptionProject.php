<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class DescriptionProject extends Model
{
    protected $table = 'description_projects';

    protected $fillable = ['id','name','project_id'];


    public function project(){
        return $this->belongsTo('App\Project');
    }
}
