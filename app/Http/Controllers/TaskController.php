<?php

namespace App\Http\Controllers;

use App\DescriptionTask;
use App\Repositories\Task\TaskRepository;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    /**
     * @var TaskRepository
    */
    private $taskRepository;

    /**
     * TaskController constructor.
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }


    /**
     * This method returns task view.
     *
     * Return view.
     */
    public function index()
    {
        return view("task.task");
    }


    /**
     * This method creates new task.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * Return make redirect on step before.
     */
    public function create(Request $request) {
        $task = $request->all();
        $this->taskRepository->createOrUpdate(
                isset($task['id_Task']) ? $task['id_Task'] : null,
                isset($task['summary']) ? $task['summary'] : null,
                isset($task['key']) ? $task['key'] : null,
                isset($task['resolution']) ? $task['resolution'] : null,
                isset($task['status']) ? $task['status'] : null,
                '#fff',
                isset($task['priority']) ? $task['priority'] : null,
                isset($task['descriptions']) ? $task['descriptions'] : null,
                isset($task['labels']) ? $task['labels'] : null,
                isset($task['project_id']) ? $task['project_id'] : null
            );


        $url = route("addTask", ["id" => $task[ 'project_id' ]]);
        return redirect($url);
    }


    /**
     * This method assigns tasks to the project
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * return json
     */
    public function edit(Request $request) {
        if ($request->isMethod('GET')) {
            return response()->json([
                'response' => $this->taskRepository->appointmentOfTasks($request['tasks'], $request['id'])
            ]);
        }
    }


    /**
     * This method makes destroy Task.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response return json
     *
     * return json
     */
    public static function destroy(Request $request) {
        $task = Task::find($request['id']);
        $task->specialities()->detach();
        $task->delete();

        DescriptionTask::where('task_id', $request['id'])->delete();

        return response()->json(['response' => true]);
    }

}
