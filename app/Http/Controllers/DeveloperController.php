<?php

namespace App\Http\Controllers;


use App\Repositories\Developer\DeveloperRepository;
use Illuminate\Http\Request;

class DeveloperController extends Controller {

    /**
     * @var TaskRepository
     */
    private $developerRepository;

    /**
     * DeveloperRepository constructor.
     * @param DeveloperRepository $developerRepository
     */
    public function __construct(DeveloperRepository $developerRepository)
    {
        $this->developerRepository = $developerRepository;
    }

    /**
     * The method checks the busy programmer in this interval of time.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function checkBusy(Request $request) {
        if ($request->isMethod('post')) {
            return response()->json([
                'response' => $this->developerRepository->checkBusy($request->start, $request->end, $request->id)
            ]);
        }
    }

    /**
     * The method uses to check the free programmer at this time.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function checkBusyDevUnderDistribution(Request $request, $id = null) {
        if ($request->isMethod('get')) {
            return response()->json([
                'response' => $this->developerRepository->checkBusyDevUnderDistribution(
                    $request->developers,
                    isset($id) ? $id : $request->id
                )
            ]);
        }
    }

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * Return json
     */
    public function appointmentOfDevelopers(Request $request) {
        if ($request->isMethod('get')) {
            return response()->json([
                'response' => $this->developerRepository->appointmentOfDevelopers($request['developers'], $request['id'])
            ]);
        }
    }

}
