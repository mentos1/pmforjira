<?php

namespace App\Http\Controllers;

use App\DescriptionTask;
use App\Developer;
use App\PriorityTask;
use App\Project;
use App\Repositories\Project\ProjectRepository;
use App\Specialie;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\JiraRequestModel\UploadJira;
use Illuminate\Support\Facades\Auth;
use App\DescriptionProject;
use App\Facades\DeveloperFacade;
use App\Facades\TaskFacade;
use App\Facades\ProjectFacade;

class ProjectController extends Controller
{

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * TaskController constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * This method returns all projects.
     * that are assigned to the developers.
     *
     * Return json.
     */
    public function index() {
        return Response()->json($this->projectRepository->getAllProjectAndDeveloper());
    }


    /**
     * This method returns all projects.
     * that are not assigned to developers.
     *
     * Return json.
     */
    public function withOutDeveloper() {
        return Response()->json($this->projectRepository->getAllProjectWithOutDeveloper());
    }


    /**
     * This method returns all developers
     *
     * Return json.
     */
    public function developer() {
        return Response()->json($this->projectRepository->getDeveloper());
    }

    /**
     * This method create or update project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return redirect to rout home.
     */
    public function store(Request $request) {
        $project = isset($request['checkbox']) ? Project::find($request['project']) : new Project();

        $this->projectRepository->create(
            $project,
            isset($request['title']) ? $request['title'] : null,
            isset($request['key']) ? $request['key'] : null,
            isset($request['projectTypeKey']) ? $request['projectTypeKey'] : null,
            isset(Auth::user()->name) ? Auth::user()->name : null,
            $request['start-date'] . " " . $request['start-time'],
            $request['end-date'] . " " . $request['end-time'],
            isset($request['distribution']) ? $request[ 'distribution'] : null
            );

        return redirect()->route('home');
    }

    /**
     * This method show project by id.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response return json
     *
     * return json
     */
    public function getProject(Request $request) {
        $project = Project::find($request->id);

        return response()->json([
            'response' => view('project.infoProject', ['project' => $project] )->render()
        ]);
    }


    /**
     * This method takes two method( Get, Post ).
     *
     * Post returns json includes view which make update or create task.
     *
     * Get returns view main page.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
     * Return view.
     */
    public function addTask(Request $request, $id = null) {

        if ($request->isMethod('post')) {
            $priority = PriorityTask::all();
            $project = Project::where('id', '=', $id)->with(['tasks', 'descriptions'])->get();
            $arrDescriptionsOnTheTask =[];


            if (isset($project[0]['descriptions'])) {
                foreach ($project[0]['descriptions'] as $description) {
                    array_push($arrDescriptionsOnTheTask, $description);
                }
            }

            if ($request['id'] != 0) {
                $task= Task::find($request['id']);
                return response()->json([
                    'response' => view('task.task', [
                        'task' => $task,
                        'project_id' => $request['project_id'],
                        'button' => 'Update',
                        'priority' => $priority,
                        'priorityTask' => $task->priority->name,
                        'arrDescriptionsOnTheTask' => $arrDescriptionsOnTheTask,
                        'specialitiesTask' => $task->specialities
                        ]
                    )->render()
                ]);
            } else {
                return response()->json([
                    'response' => view( 'task.task', [
                        'task' => new Task(),
                        'project_id' => $request[ 'project_id' ],
                        'button' => 'Add',
                        'priority' => $priority,
                        ]
                    )->render()
                ] );
            }
        }


        if ($request->isMethod('get')) {
            $project = Project::where('id', '=', $id)->with(['tasks', 'descriptions'])->get();
            $arrTaskOnTheProject =[];
            $arrDescriptionsOnTheTask =[];


            if (isset($project[0]['descriptions'])) {
                foreach ($project[0]['descriptions'] as $description) {
                    array_push($arrDescriptionsOnTheTask, $description);
                }
            }

            if (isset($project[0]['tasks'])) {
                foreach ($project[0]['tasks'] as $task) {
                    array_push($arrTaskOnTheProject, $task);
                }
            }


            $data = [
                'tasks' => Task::get(['id', 'summary','project_id']),
                'project' => Project::find($id),
                'arrDescriptionsOnTheTask' => $arrDescriptionsOnTheTask,
                'arrTaskOnTheProject' => $arrTaskOnTheProject
            ];
            return view("task.addTask",$data);
        }
    }

    /**
     * This method takes two method( Get, Post ).
     *
     * Post returns json includes view.
     * Get returns view main page.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function addDev(Request $request, $id = null)
    {

        if ($request->isMethod('post')) {
            $developer = Developer::find($request['id']);
            return response()->json([
                'response' => view('developer.developer', ['developer' => $developer, 'level' => $developer->level->name])->render()
            ]);
        }


        if ($request->isMethod('get')) {
            $arrFreeDeveloperOnTheProject =[];
            $arrDescriptionsOnTheProject =[];

            $project = Project::where('id', '=', $id)->with(['developers','descriptions'])->get(); //where('id', '=', $id)->with(['',''])->get()
            $developers = Developer::get(['id', 'title']);




            if (isset($project[0]['descriptions'])) {
                foreach ($project[0]['descriptions'] as $description) {
                    array_push($arrDescriptionsOnTheProject, $description);
                }
            }


            if (isset($project[0]['developers'])) {
                foreach ($project[0]['developers'] as $developer) {
                    array_push($arrFreeDeveloperOnTheProject, $developer);
                    $developers = $this->findInCollectionAndDrop($developers, 'id',  $developer['id']);
                }
            }




            $data = [
                'arrDeveloperOnTheProject' => $developers,
                'project' => Project::find($id),
                'developers' => $arrFreeDeveloperOnTheProject,
                'arrDescriptionsOnTheProject' => $arrDescriptionsOnTheProject
            ];
            return view( "developer.addDeveloper", $data );
        }

    }


    /**
     * This method update project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return json.
     */
    public function projectShow(Request $request) {
        $this->projectRepository->updateDateProject($request->id,  $request->start, $request->end);
        return response()->json(['response' => true ]);
    }

    /**
     * This method update project when date and time was separated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * return json.
     */
    public function updateWithSeparateDate(Request $request) {
            $project = Project::find($request['id']);
            $this->projectRepository->create(
                $project,
                isset($request[ 'title' ]) ? $request[ 'title' ] : null,
                Auth::user()->name,
                isset($request[ 'key' ]) ? $request[ 'key' ] : null,
                isset($request[ 'projectTypeKey' ]) ? $request[ 'projectTypeKey' ] : null,
                $request['start-date'] . " " . $request['start-time'],
                $request['end-date'] . " " . $request['end-time'],
                isset($request[ 'distribution' ]) ? $request[ 'distribution' ] : null
            );

            return redirect()->route('home');
    }

    /**
     * This method update date-end project when the project size changes in the calendar.
     *
     * @param  \Illuminate\Http\Request $request
     * @param integer $id
     * @return \Illuminate\Http\Response return json.
     *
     * return json.
     */
    public function updateResize(Request $request, $id) {
            $msg = Project::find($id);
            $end = $msg->end;
            $resEnd = Carbon::createFromFormat('Y-m-d H:i:s', $end);
            $msg->end = $resEnd->addSeconds($request->dayDelta);
            $msg->save();
            return response()->json(['response' => true]);
    }

    /**
     * This method destroy Project by id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $project = Project::find($id);

        $project->developers()->detach();
        $project->delete();

        DescriptionProject::where('project_id', $id)->delete();

        return response()->json(['response' => true]);
    }

    /**
     * This method find the value in the Collection.
     *
     * @param $arrCollection
     * @param $attr
     * @param $val
     * @return \Illuminate\Http\Response
     */
    function findInCollection($arrCollection, $attr, $val) {
        foreach ($arrCollection as $it) {
            if ($it[$attr] == $val) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method find and drop the value in the Collection.
     *
     * @param $arrCollection
     * @param $attr
     * @param $val
     * @return \Illuminate\Http\Response
     */
    function findInCollectionAndDrop($arrCollection, $attr, $val) {
        if(isset($arrCollection))
            for ($i = 0; $i < count($arrCollection); $i++) {
                if (isset($arrCollection[$i]) && $arrCollection[$i][$attr] == $val) {
                    unset($arrCollection[$i]);
                }
            }
        return $arrCollection;
    }

    /**
     * This method destroy Project by id.
     *
     * @param $labels
     * @return \Illuminate\Http\Response
     */
    function getUniqueLabelsId($labels) {
        if (isset($labels)) {
            $arrIdSpeciality = Specialie::whereIn('name', $labels)->pluck('id')->toArray(); // new whereIn
            $arrNameSpeciality = Specialie::whereIn('name', $labels)->pluck('name')->toArray(); // new whereIn
            $arrUnique = array_diff($labels, $arrNameSpeciality);

            $data = array();
            foreach ($arrUnique as $item) {
                array_push($data, ["name" => $item]);
            }

            Specialie::insert($data);

            return array_merge($arrIdSpeciality, Specialie::whereIn('name', $arrUnique)->pluck('id')->toArray());
        }
    }

    /**
     * This method make download from Jira and add to Db
     *
     * @return \Illuminate\Http\Response
     */
    public function download() {

        //------------------Projects----------------------//
        $projects = UploadJira::searchForUploadProject();
        if (isset($projects)) {
            $arrProjectName =[];
            $arrProjects =[];

            foreach ($projects as $project){
                array_push($arrProjectName, $project->name);
            }

            $foundProjects = Project::whereIn('title', $arrProjectName)->get();

            foreach ($projects as $project) {
                if($this->findInCollection($foundProjects, 'title',  $project->name)) {
                    array_push($arrProjects, [
                        'title' => $project->name,
                        'key' => $project->key,
                        'projectTypeKey' => $project->projectTypeKey,
                        'lead' => $project->lead->displayName,
                    ]);
                }
            }
            Project::insert($arrProjects);
        }

        //------------------Developer----------------------//
        $responseDevelopers = array();
        $uploadProgrammer = UploadJira::searchUserForUpload();
        $d = null;
        foreach ($uploadProgrammer as $key => $val) {
            if ($key % 2 == 0) {
                $d = new \stdClass();
                $d->displayname = $val;
            } else {
                $d->email = $val;
                array_push($responseDevelopers, $d);
            }
        }
        $developers = $responseDevelopers;

        //array resourceId Developers
        $arrResourceIdDeveloper = [];
        $arrDevelopers = [];
        foreach ($developers as $developer) {
            array_push($arrResourceIdDeveloper, $developer->email);
        }
        $foundDevelopers = Developer::whereIn('resourceId', $arrResourceIdDeveloper)->get();

        foreach ($developers as $developer) {
            if ($this->findInCollection($foundDevelopers, 'resourceId',  $developer->email)) {
                array_push($arrDevelopers, [
                    'title' => $developer->displayname,
                    'resourceId' => $developer->email
                ]);
            }
        }
        Developer::insert($arrDevelopers);


        //------------------Task----------------------//
        $tasks = UploadJira::searchTaskForUpload();
        
        if (isset($tasks)) {
            $arrTaskSummary =[];
            $arrTasks =[];
            $arrDescriptions=[];

            //create array summary
            foreach ($tasks as $task) {
                array_push($arrTaskSummary, $task->fields->summary);
            }

            $foundTasks = Task::whereIn('summary', $arrTaskSummary)->get();

            //add Tasks
            foreach ($tasks as $task) {
                if($this->findInCollection($foundTasks, 'summary',  $task->fields->summary)) {
                    array_push($arrTasks, [
                        'summary' => $task->fields->summary,
                        'key' => $task->key,
                        'resolution' => $task->fields->resolution,
                        'status' => $task->fields->status->name,
                        'color' => '#fff',
                        'priority_id' => PriorityTask::where('name', $task->fields->priority->name)->first()->id,
                        'project_id' => 1
                    ]);
                }
            }
            Task::insert($arrTasks);


            //add Description and Label Tasks
            $foundTasks = Task::whereIn('summary', $arrTaskSummary)->get();
            $i = 0;
            foreach ($tasks as $task) {
                if($this->findInCollection($foundTasks, 'summary',  $task->fields->summary)) {

                    //create array description
                    array_push($arrDescriptions, [
                        'name' => isset($task->fields->description) ? $task->fields->description : "",
                        'task_id' => $foundTasks[$i],
                    ]);

                    //add labels Tasks
                    Task::find($foundTasks[$i])
                        ->specialities()
                        ->attach($this->getUniqueLabelsId($task->fields->labels));

                    $i++;
                }
            }
            DescriptionTask::insert($arrDescriptions);
        }


        return response()->json(['response' => true]);

    }

    /**
     * This method return view
     *
     * @return \Illuminate\Http\Response
     *
     * return main of project view
     */
    public function reloadProjectFormDb() {
        $date = [
            'project' => Project::whereNull('start')->get(['id', 'title'])
        ];
        return view('project.distribution', $date);
    }

    /**
     * This method sends data on view for rendering.
     *
     * @param $id
     * @return \Illuminate\Http\Response return main of project view
     *
     * return json with rendered view.
     */
    public function updateProject($id) {

        $projectById = Project::find($id);

        $project = new \stdClass();
        $project->id = isset($projectById->id) ?  $projectById->id :  "";
        $project->title = isset($projectById->title) ? $projectById->title :  "";

        $date = explode(" ", $projectById->start );
        $project->startD = isset($date[0]) ? $date[0] :  "";
        $project->startT = isset($date[1]) ? $date[1] :  "";

        $date = explode(" ", $projectById->end );
        $project->endD = isset($date[0]) ? $date[0] : "";
        $project->endT = isset($date[1]) ? $date[1] : "";

        $project->descriptions = isset($projectById->descriptions[0]->name) ? $projectById->descriptions[0]->name : "";

        return response()->json([
            'response' => view('project.updateProject', ['project' => $project])->render()
        ]);
    }

    /**
     * This method makes render view.
     *
     * @return \Illuminate\Http\Response
     *
     * return view
     */
    public function reloadTaskFormDb() {
        $project = Project::whereNull('start')->get( ['id', 'title', 'start', 'end']);

        return response()->json([
            'response' => view('project.createProject', ['project' => $project])->render()
        ]);
    }
}
