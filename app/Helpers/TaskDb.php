<?php


namespace App\Helpers;

use App\DescriptionTask;
use App\Helpers\Contracts\TaskInterface;
use App\Project;
use App\Specialie;
use App\Task;
use Illuminate\Http\Request;


class TaskDb implements TaskInterface {


    /**
     * This method assigns tasks to the project.
     *
     * @param array $arrTask
     * @param integer $id
     * @return array
     *
     * Returns an array of tasks assigned to the project
     */
    public static function appointmentOfTasks( $arrTask, $id )
    {
            $project= Project::find($id);
            $arrId = [];
            foreach ($project->tasks as $task) {
                array_push($arrId , $task->id);
            }

            //reset
            if (count($arrId) != 0) {
                $tasks = Task::whereIn('id',$arrId);
                $tasks->update(['project_id' => 1]);
            }

            //set
            if (count($arrTask) != 0) {
                $tasks = Task::whereIn('id',$arrTask);
                $tasks->update(['project_id' => $id]);
            }



            return $project->tasks;
    }


    /**
     * This method creates or updates a task.
     *
     * @param integer $id
     * @param string $summary
     * @param string $key
     * @param string $resolution
     * @param string $status
     * @param string $color
     * @param int $priorityId
     * @param string $descriptions
     * @param string $labels
     * @param int $projectId
     *
     * Nothing return
     */
    public static function createOrUpdate(
          $id,
          $summary,
          $key,
          $resolution,
          $status,
          $color,
          $priorityId,
          $descriptions,
          $labels,
          $projectId = 1
    ) {
        if ($id != null) {
            $newTask = Task::find($id);
        }
        else {
            $newTask = new Task();
            $newTask->project_id = $projectId;
        }

        $newTask->summary = $summary;
        $newTask->key = $key;
        $newTask->resolution = $resolution;
        $newTask->status = $status;
        $newTask->color = $color;
        $newTask->priority_id = $priorityId;

        $newTask->save();

        if ($descriptions !== null) {
            $desTask = new DescriptionTask();
            $desTask->name = $descriptions;
            $desTask->task_id = $newTask->id;
            $desTask->save();
        }

        $result = [];
        if (isset($labels)) {
            $arrIdSpeciality = Specialie::whereIn('name', $labels)->pluck('id')->toArray(); // new whereIn
            $arrNameSpeciality = Specialie::whereIn('name', $labels)->pluck('name')->toArray(); // new whereIn
            $arrUnique = array_diff($labels, $arrNameSpeciality);

            $data = array();
            foreach ($arrUnique as $item) {
                array_push($data, ["name" => $item]);
            }

            Specialie::insert($data);
            $result = array_merge($arrIdSpeciality, Specialie::whereIn('name', $arrUnique)->pluck('id')->toArray());
        }

        $taskSpeciality = Task::find($newTask->id);
        $taskSpeciality->specialities()->detach();
        $taskSpeciality->specialities()->attach($result);
    }
}