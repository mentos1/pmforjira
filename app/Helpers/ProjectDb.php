<?php


namespace App\Helpers;

use App\DescriptionProject;
use App\Developer;
use App\Helpers\Contracts\ProjectInterface;
use App\Project;


class ProjectDb implements ProjectInterface {

    /**
     * This method returns all projects
     * that are assigned to the developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectAndDeveloper()
    {
        $arr = [];
        $developers = Developer::with('projects')->get(['id', 'email']);

        //with a resourceId
        foreach ($developers as $developer) {
            if (isset($developer->projects)) {
                foreach ($developer->projects as $it) {
                    $obj = new \stdClass();
                    $obj->id = $it->id;
                    $obj->title = $it->name; //не использую toArray из-за другоuо названия поля
                    $obj->start = $it->start;
                    $obj->end = $it->end;
                    $obj->resourceId = $developer->email; //не использую toArray из-за другоuо названия поля
                    array_push($arr, $obj);
                }
            }
        }

        $projects = Project::with('developers')->get(['id', 'name', 'start', 'end']);

        //with out a resourceId
        foreach($projects as $it) {
            if (!sizeof($it->developers)) {
                $obj = new \stdClass();
                $obj->id = $it->id;
                $obj->title = $it->name; //не использую toArray из-за другоuо названия поля
                $obj->start = $it->start;
                $obj->end = $it->end;

                array_push($arr, $obj);
            }
        }
        return $arr;
    }

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectWithOutDeveloper()
    {

         $projects = Project::get(['id', 'name', 'start', 'end']);

         foreach($projects as $project) {
             (new self())->change_key('name', 'title', $project);
         }

         return $projects;
    }

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @param string $key
     * @param string $new_key
     * @param array $arr
     * @param bool $rewrite
     * @return boolean successful or no successful action
     *
     * Returns an array projects
     */
    function change_key($key, $new_key, $arr, $rewrite=true)
    {
        if(!array_key_exists($new_key,$arr) || $rewrite) {
            $arr[$new_key]=$arr[$key];
            unset($arr[$key]);
            return true;
        }
        return false;
    }

    /**
     * This method returns all developers
     *
     * @return array
     *
     * Returns an array developers
     */
    public static function getDeveloper()
    {
        $arr = [];
        $developers = Developer::get(['id', 'email','title']);
        foreach ($developers as $developer) {
            $obj = new \stdClass();
            $obj->id = $developer->email; //не использую toArray из-за другоuо названия поля
            $obj->title = $developer->title; //не использую toArray из-за другоuо названия поля
            array_push ($arr, $obj);
        }
        return  $arr;
    }

    /**
     * This method create project
     *
     * @param Project $project
     * @param string $name
     * @param string $key
     * @param string $projectTypeKey
     * @param string $lead
     * @param string $start
     * @param string $end
     * @param null $descriptions
     *
     * Nothing returns
     */
    public static function create(
          $project,
          $name,
          $key,
          $projectTypeKey,
          $lead,
          $start = '1970-01-01 00:00:00',
          $end = '1970-01-01 00:00:00',
          $descriptions = null
    ) {

        $name != null ? $project->name = $name : false;
        $lead != null ? $project->lead = $lead : false;
        $key != null ? $project->key = $key : false;
        $projectTypeKey !== null ? $project->projectTypeKey = $projectTypeKey : false;
        $project->start = $start;
        $project->end = $end;
        $project->save();

        if ($descriptions != null) {
            DescriptionProject::where('project_id', $project->id)->delete();
            $descriptionNew = new DescriptionProject();
            $descriptionNew->name = $descriptions;
            $descriptionNew->project_id = $project->id;
            $descriptionNew->save();
        }
    }

    /**
     * This method update project
     *
     * @param $id
     * @param string $start
     * @param string $end
     *
     * Nothing returns
     */
    public static function updateDateProject($id,  $start, $end)
    {
        $msg = Project::find($id);
        $msg->start = $start;
        $msg->end = $end;
        $msg->save();
    }

}
