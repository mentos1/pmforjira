<?php


namespace App\Helpers;

use App\Developer;
use App\Helpers\Contracts\DeveloperInterface;
use App\Project;
use Carbon\Carbon;


class DeveloperDb implements DeveloperInterface {


    /**
     * When you change the date of the project for which it has developers.
     * The method is called "checkBusy" checking the busy programmer in this interval of time.
     *
     * @param string $start
     * @param string $end
     * @param integer $id
     * @return boolean
     *
     * Return boolean value
     */
    public static function checkBusy($start, $end, $id)
    {
        $thisObj = new self();
        $start =  $thisObj->CreateDateFormat($start);
        $end =  $thisObj->CreateDateFormat($end);

        $arrDev = Project::find($id)->developers->pluck('id')->toArray();
        $developers = $thisObj->getBusyDevelopers($arrDev, $start, $end, $id);

        if (count($developers) !== 0) {
            return false;
        }

        return true;
    }


    /**
     * This method makes the date format Y-m-d H:i:s
     *
     * @param string $dateCreate
     * @return carbon
     *
     * Return carbon data
     */
    public static function CreateDateFormat($dateCreate)
    {
        $date = explode(" ", $dateCreate);
        $startTime = isset($date[1]) ? $date[1] : "";
        $arrTime = explode(":", $startTime);
        $start = "0000-00-00 00:00:00.000000";

        if (count($arrTime) == 0) $start = Carbon::createFromFormat('Y-m-d', $dateCreate);

        if (count($arrTime) == 1) $start = Carbon::createFromFormat('Y-m-d H', $dateCreate);

        if (count($arrTime) == 2) $start = Carbon::createFromFormat('Y-m-d H:i', $dateCreate);

        if (count($arrTime) == 3) $start = Carbon::createFromFormat('Y-m-d H:i:s', $dateCreate);
        return $start;
    }

    /**
     * When the developer is assigned to the project,
     * the method "checkBusyDevUnderDistribution"
     * is used to check the free programmer at this time.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function checkBusyDevUnderDistribution($arrDeveloper,$id)
    {
        $thisObj = new self();
        $arr = [];

        if (isset($arrDeveloper)) {
            $start = $thisObj->CreateDateFormat(Project::find($id)->start);
            $end = $thisObj->CreateDateFormat(Project::find($id)->end);
            $developers = $thisObj->getBusyDevelopers($arrDeveloper, $start, $end, $id);
            foreach($developers as $item) {
                array_push($arr, $item->title);
            }
        }

        return $arr;
    }

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param carbon $start
     * @param carbon $end
     * @param integer $id
     * @return array Returns an array with busy developers
     *
     * Returns an array with busy developers
     */
    function getBusyDevelopers($arrDeveloper, $start, $end, $id)
    {
        return Developer::whereIn("id", $arrDeveloper)
            ->with('projects')
            ->whereHas('projects', function($q) use($start, $end, $id)
            {
                $q->where([
                    ['projects.id','<>', $id],
                    ['projects.start','>=', $start],
                    ['projects.start','<=', $end],
                ])
                ->orWhere([
                    ['projects.id','<>', $id],
                    ['projects.end', '>=', $start],
                    ['projects.end', '<=', $end],
                ]);
            })
            ->get();
    }


    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function appointmentOfDevelopers($arrDeveloper, $id)
    {
        $project = Project::find($id);
        $project->developers()->detach();
        $project->developers()->attach($arrDeveloper);
        return $project->developers;
    }


}