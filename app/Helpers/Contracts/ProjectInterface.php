<?php

namespace App\Helpers\Contracts;

use App\Project;

Interface ProjectInterface {

    /**
     * This method returns all projects
     * that are assigned to the developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectAndDeveloper();

    /**
     * This method returns all projects
     * that are not assigned to developers.
     *
     * @return array
     *
     * Returns an array projects
     */
    public static function getAllProjectWithOutDeveloper();

    /**
     * This method returns all developers
     *
     * @return array
     *
     * Returns an array developers
     */
    public static function getDeveloper();

    /**
     * This method create project
     *
     * @param Project $project
     * @param string $name
     * @param string $key
     * @param string $projectTypeKey
     * @param string $lead
     * @param string $start
     * @param string $end
     * @param $distribution
     *
     *
     * @return
     * Nothing returns
     */
    public static function create(
        $project,
        $name,
        $key,
        $projectTypeKey,
        $lead,
        $start,
        $end,
        $distribution
    );

    /**
     * This method update project
     *
     * @param $id
     * @param string $start
     * @param string $end
     *
     * Nothing returns
     */
    public static function updateDateProject($id,  $start, $end);
}
