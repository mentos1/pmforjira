<?php

namespace App\Helpers\Contracts;


Interface TaskInterface {

    /**
     * This method assigns tasks to the project.
     *
     * @param array $arrTask
     * @param integer $id
     * @return array
     *
     * Returns an array of tasks assigned to the project
     */
    public static function appointmentOfTasks( $arrTask, $id );

    /**
     * This method creates or updates a task.
     *
     * @param integer $id
     * @param string $summary
     * @param string $key
     * @param string $resolution
     * @param string $status
     * @param string $color
     * @param integer $priorityId
     * @param integer $projectId
     * @param string $descriptions
     * @param string $labels
     *
     * Nothing returns
     */
    public static function createOrUpdate(
        $id ,
        $summary,
        $key,
        $resolution,
        $status,
        $color,
        $priorityId,
        $projectId,
        $descriptions,
        $labels
    );
}
