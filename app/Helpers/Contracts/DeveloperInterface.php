<?php

namespace App\Helpers\Contracts;


Interface DeveloperInterface {
    /**
     * When you change the date of the project for which it has developers.
     * The method is called "checkBusy" checking the busy programmer in this interval of time.
     *
     * @param string $start
     * @param string $end
     * @param integer $id
     * @return boolean
     *
     * Return boolean value
     */
    public static function checkBusy($start, $end, $id);

    /**
     * This method makes the date format Y-m-d H:i:s
     *
     * @param string $dateCreate
     * @return carbon
     *
     * Return carbon data
     */
    public static function CreateDateFormat($dateCreate);

    /**
     * When the developer is assigned to the project,
     * the method "checkBusyDevUnderDistribution"
     * is used to check the free programmer at this time.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function checkBusyDevUnderDistribution($arrDeveloper,$id);

    /**
     * This method makes the
     * assignment of developers to the project.
     *
     * @param array $arrDeveloper
     * @param integer $id
     * @return array
     *
     * Returns an array with busy developers
     */
    public static function appointmentOfDevelopers($arrDeveloper, $id);

}
