<div class="modal-header">
    <h3>{{ $button }} Task</h3>
</div>
<form method="get" action="{{ route('task.create') }}" id="form1">
    <input type="hidden" name="id_Task"  value="{{ isset($task->id) ? $task->id : '' }}">
    <input type="hidden" name="project_id" value="{{ isset($project_id ) ? $project_id  : '' }}">
    <div class="modal-body">

    <p><b>Summary<span style="color:red">*</span></b><input name="summary"  class="form-control form-control-sm widthMax" value="{{ isset($task->summary) ? $task->summary : '' }}" required></p>
    <p><b>Key</b><input class="widthMax"  name="key" value="{{ isset($task->key) ? $task->key : '' }}"></p>
    <p><b>Priority<span style="color:red">*</span></b>
        <select class="widthMax selectpicker show-tick" name="priority"  required>
                @if(isset($task->priority->name))
                    @foreach($priority as $it)
                        @if($priorityTask != $it->name)
                            <option value="{{ $it->id }}">{{ $it->name }}</option>
                        @else
                            <option selected value="{{ $it->id }}">{{ $it->name }}</option>
                        @endif
                    @endforeach
                @else
                    @foreach($priority as $it)
                            <option value="{{ $it->id }}">{{ $it->name }}</option>
                    @endforeach
                @endif
        </select>
    </p>
    <p>
        <b>Descriptions</b>
        @if(isset($task->descriptions[0]->name))
            <div class="widthMax layer">
                    @foreach($arrDescriptionsOnTheTask as $descriptions)
                         <textarea name="descriptions" class="form-control" rows="5">{{ isset($descriptions->name) ? $descriptions->name : "" }}</textarea>
                    @endforeach
            </div>
        @else
            <textarea class="form-control" name="descriptions" rows="5"></textarea>
        @endif
    </p>
    <p>
        <b>Resolution</b>
        <textarea name="resolution" class="form-control" rows="5">{{ isset($task->resolution) ? $task->resolution : "" }}</textarea>
    </p>
    <p>
    @if(isset($task->resolution))
        <b>Status</b>
           <input class="widthHalf" name="status" type="text" value="{{ isset($task->status) ? $task->status : "" }}">
    @else
        <b>Status<span style="color:red">*</span></b>
        <input class="widthHalf" name="status" type="text" required>
    @endif
    </p>
    <p>
    @if(isset($task->specialities[0]->name))
        <b>Label</b>
            <input name="jurors" id="jurors" type="text" style="width: 50%;">
            <input type="button" value="->" class="addButton" style="width: 10%;" onclick="add_D();" /> <!-- The 'onclick="add();" will call a the Javascript function add() -->
            <select id="mySelect" name="labels[]" style="width:30%; vertical-align: top; border: 1px solid #b4b4b4; border-radius: 10px" rows="1" multiple="multiple">
            @foreach($specialitiesTask as $specialities)
                <option value="{{ $specialities->name }}">{{ $specialities->name }}</option>
            @endforeach
            </select>
        </b>
    @else
        <b style="width: 10%">Label:</b>
        <input name="jurors" id="jurors" type="text" style="width: 50%;">
        <input type="button" value="->" class="addButton" style="width: 10%;" onclick="add_D();" /> <!-- The 'onclick="add();" will call a the Javascript function add() -->
        <select id="mySelect"  name="labels[]" style="width:30%; vertical-align: top; border: 1px solid #b4b4b4; border-radius: 10px" rows="1" multiple="multiple"></select>
    @endif
    </p>
</div>
<div class="modal-footer">
    <button class ='btn btn-default' id="close">Close</button>
    @if($button == 'Update')
        <button class ='btn btn-danger' data-id ="{{ isset($task->id) ? $task->id : "" }}" id="delete">Delete</button>
    @endif
    <button class ='btn btn-info'  id="add">{{ $button }}</button>
</div>
</form>
