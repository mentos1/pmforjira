{!! Form::open(['route' => 'project.store', 'method' => 'post']) !!}
    <div class="modal-dialog">
        <div class="modal-header">
            <h3>Add Project</h3>
        </div>
        <div class="modal-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Title<span style="color:red">*</span></label>
                <div class="col-sm-4">
                    <input type="text" id="nameProject" class="form-control" name="title" placeholder="Name">
                </div>
                <label class="col-sm-2 col-form-label">Title from Jira</label>
                <div class="col-sm-3">
                    <select name="project" id="project">
                        @if(isset($project))
                            @foreach($project as $it)
                                <option value="{{ $it->id }}">{{ $it->title }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <input type="checkbox" name="checkbox"  id="open_select_from_jira" class="col-sm-1">
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Start<span style="color:red">*</span></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control datepicker" id="start-date" required name="start-date" placeholder="Date">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control ui-timepicker-input" required value="10:00" name="start-time">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">End<span style="color:red">*</span></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control datepicker" id="end-date" required name="end-date" placeholder="Date">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control ui-timepicker-input" required value="19:00" name="end-time">
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Distribution</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="5" name="distribution" id="comment"></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    {!! Form::close() !!}
