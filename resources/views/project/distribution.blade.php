@extends('layouts.app')

@section('style')
    @parent
@endsection

@section('script')
    @parent
@endsection


@section('myStyle')
    @parent
    <style>
        #downloadFromJira{
            float: right;
            width: 20px;
            height: 20px;
        }
        #print{
            margin-right: 3%;
            float: right;
            width: 20px;
            height: 20px;
        }
        .alert{
            display: none;
        }
    </style>
@endsection

@section('preloader')
    @parent
@endsection

@section('navBar')
    @parent
@endsection

@section('content')
    @parent
@endsection

@section('myScript')
    @parent
@endsection