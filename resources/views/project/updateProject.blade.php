{!! Form::open(['route' => 'updateWithSeparateDate', 'method' => 'post']) !!}
<div class="modal-dialog">
    <input type="hidden" name="id" value="{{ $project->id }}">
    <div class="modal-header">
        <h3>Add Project</h3>
    </div>
    <div class="modal-body">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Title<span style="color:red">*</span></label>
            <div class="col-sm-4">
                <input type="text" id="nameProject" class="form-control" required value="{{ isset($project->title) ? $project->title : "" }}" name="title" placeholder="Name">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Start<span style="color:red">*</span></label>
            <div class="col-sm-5">
                <input type="text" class="form-control datepicker" id="start-date" name="start-date" value="{{ isset($project->startD) ? $project->startD : "" }}" placeholder="Date">
            </div>
            <div class="col-sm-5">
                    <input type="text" class="form-control ui-timepicker-input" required value="{{ isset($project->startT) ? $project->startT : "" }}"  name="start-time">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">End<span style="color:red">*</span></label>
            <div class="col-sm-5">
                <input type="text" class="form-control datepicker" required id="end-date" value=" {{ isset($project->endD) ? $project->endD : "" }}" name="end-date" placeholder="Date">
            </div>
            <div class="col-sm-5">
                <input type="text" class="form-control ui-timepicker-input" value="{{ isset($project->endT) ? $project->endT : "" }}" required  name="end-time">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Distribution</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" name="distribution" id="comment">{{ isset($project->descriptions) ? $project->descriptions : "" }}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
    </div>
</div>
{!! Form::close() !!}