<div class="form-group row">
    <div class="container-fluid">
        <div class="col-md-6">
            <b>Title:</b><span id="title">{{ isset($project->title)  ? $project->title : "" }}</span></br>
            <b>Lead:</b><span id="lead">{{ isset($project->lead) ? $project->lead : "" }}</span></br>
            <b>Start:</b><span id="start">{{ isset($project->start) ? $project->start : "" }}</span>
            <b>End:</b><span id="end">{{ isset($project->end) ? $project->end : "" }}</span></br>
        </div>
        <div class="col-md-3">
            <p>Developers <img class="add_img"  data-url = "{{ route('addDev', ['id' => isset($project->id) ? $project->id : 1]) }}" src="{{ asset('/public/img/plus-2.png') }}"></p>
            <ul style="padding-left: 0">
                @if(isset($project->developers))
                    @foreach($project->developers as $pr)
                        <li>{{ $pr->title }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-md-3">
            <p>Tasks <img class="add_img" data-url = "{{ route('addTask', ['id' => isset($project->id) ? $project->id : 1]) }}" src="{{ asset('/public/img/plus-2.png') }}"></p>
            <ul style="padding-left: 0">
                @if(isset($project->tasks))
                    @foreach($project->tasks as $pr)
                        <li>{{ $pr->summary }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
        <button class ='btn btn-danger' data-url = "{{ route('project.destroy', ['id' => isset($project->id) ? $project->id : 1]) }}" id="delete">Delete</button>
        <button class ='btn btn-info' data-url = "{{ route('updateProject', ['id' => isset($project->id) ? $project->id : 1]) }}"  id="updateInfo">Update Information</button>
</div>
